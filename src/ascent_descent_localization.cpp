
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/subscriber.h>
#include "opencv2/calib3d.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <cv_bridge/cv_bridge.h>
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/ximgproc/disparity_filter.hpp"

#include <opencv2/viz.hpp>


using namespace std;
using namespace cv;


const string window_name = "view";

Mat cam_pos;

vector<KeyPoint> prev_kp;
Mat prev_img;
Mat prev_desc;

Mat kitten;

void readme();
void imageCallback(const sensor_msgs::ImageConstPtr &, const sensor_msgs::CameraInfoConstPtr &);

/**
 * @function main
 * @brief Main function
 */
int main(int argc, char **argv) {
    if (argc < 2) {
        readme();
        return -1;
    }
    string camera_name = argv[1];

    ros::init(argc, argv, "asc_localizer");
    ros::NodeHandle nh;
    namedWindow(window_name);
    startWindowThread();

    message_filters::Subscriber<sensor_msgs::Image> img_sub(nh, camera_name + "/image_rect_color", 1);
    message_filters::Subscriber<sensor_msgs::CameraInfo> info_sub(nh, camera_name + "/camera_info", 1);

    typedef message_filters::sync_policies::ExactTime<sensor_msgs::Image, sensor_msgs::CameraInfo> MySyncPolicy;
    // ExactTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
    message_filters::Synchronizer<MySyncPolicy> sync(MySyncPolicy(10), img_sub, info_sub);
    sync.registerCallback(boost::bind(&imageCallback, _1, _2));


    // load a picture of a kitten for visualizing the homography matrix
    kitten = imread("/home/darren/Pictures/kitten.jpeg", CV_LOAD_IMAGE_COLOR);
    kitten.convertTo(kitten, CV_8UC3);
    if(!kitten.data )
    {
        cout <<  "Could not open or find the image" << std::endl;
    }


    cam_pos = Mat::eye(3, 4, CV_32F);

    // visualizer for the global camera pose
    viz::Viz3d myWindow("Global Pose");
    myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());

    viz::WCube cube_widget(Point3f(0.5,0.5,0.0), Point3f(0.0,0.0,-0.5), true, viz::Color::blue());
    cube_widget.setRenderingProperty(viz::LINE_WIDTH, 4.0);

    /// Display widget (update if already displayed)
    myWindow.showWidget("Cube Widget", cube_widget);



    ros::Rate r(10);
    // trying to keep the view window responsive when images are no longer published
    while(ros::ok()) {
        ros::spinOnce();

        Mat rot_vec, t = cam_pos.col(3) * 0.1;
        Rodrigues(cam_pos.colRange(0,3), rot_vec);
        Affine3f pose(rot_vec, t);
        // cout << "rot_vec: \n" << rot_vec << endl;
        //cout << "t: \n" << cam_pos.col(3) << endl;
        myWindow.setWidgetPose("Cube Widget", pose);
        myWindow.spinOnce(1, false);

        waitKey(1);
        r.sleep();
    }

    destroyWindow("view");
    return 0;
}


void imageCallback(const sensor_msgs::ImageConstPtr &img_msg,
                   const sensor_msgs::CameraInfoConstPtr &cam_msg) {

    //cout << "got an image!" << endl;

    cv_bridge::CvImageConstPtr image_ptr;
    try {
        image_ptr = cv_bridge::toCvShare(img_msg, "bgr8");
    }
    catch (cv_bridge::Exception &e) {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", img_msg->encoding.c_str());
        return;
    }


    //-- 1. Read the images

    Mat img, imgGrey;
    Mat(image_ptr->image).convertTo(img, CV_8UC3);


    if (img.empty()) {
        ROS_ERROR("Could not read images.");
        return;
    }

    Mat img_small;
    //Show a tiny, unmodified view of the image
    resize(img, img_small, Size(), 0.3, 0.3);
    imshow(window_name, img_small);


    using namespace cv::xfeatures2d;

    //Get cropped image. Too many features were being found on the border boundary.
    int offset_x = 210;
    int offset_y = 120;

    cv::Rect roi;
    roi.x = offset_x;
    roi.y = offset_y;
    roi.width = img.size().width - (offset_x * 2);
    roi.height = img.size().height - (offset_y * 2);

    cv::Mat img_crop = img(roi);


    //experimenting with resizing. We might be able to get similar performance faster by running a smaller image
    resize(img_crop, img_crop, Size(), 0.8, 0.8);

    // enhance the contrast
    Mat img_crop_eq;
    cvtColor(img_crop, img_crop_eq, COLOR_RGB2YUV);
    Mat y_eq;
    Mat yuv[3];
    split(img_crop_eq, yuv);

    equalizeHist(yuv[0], y_eq);

    yuv[0] = y_eq;
    merge(yuv, 3, img_crop_eq);
    cvtColor(img_crop_eq, img_crop, COLOR_YUV2RGB);



    // add blur to hide the compression artifacts that are enhanced from the previous step
    for (int i = 1; i < 11; i = i + 2) {
        GaussianBlur(img_crop, img_crop, Size(i, i), 0, 0);
    }



    // Detect the keypoints using SURF Detector
    int minHessian = 40;

    //Ptr<SURF> detector_surf = SURF::create(minHessian);
    Ptr<SURF> detector_surf = SURF::create(minHessian, 4, 3, false, true);
    //Ptr<ORB> detector_orb = ORB::create(500);

    std::vector<KeyPoint> keypoints_surf;

    detector_surf->detect(img_crop, keypoints_surf);



    float resp_max = keypoints_surf[0].response,
            resp_min = keypoints_surf[0].response,
            resp_sum = 0;

    for (int i = 0; i < keypoints_surf.size(); ++i) {
        float resp = keypoints_surf[i].response;

        if (resp > resp_max) {
            resp_max = resp;
        }
        if (resp < resp_min) {
            resp_min = resp;
        }
        resp_sum += resp;
    }


    //remove keypoints with a low octave
    vector<KeyPoint> kp;
    for (int i = 0; i < keypoints_surf.size(); ++i) {

        if (keypoints_surf[i].octave >= 1) {
            kp.push_back(keypoints_surf[i]);
        }
    }


    if (kp.size() < 1) {
        //cout << "no points!" << endl;
    }


    //feature matching

    //compute feature descriptors
    Mat desc;
    detector_surf->compute(img_crop, kp, desc);

    if (prev_kp.size() > 0 && kp.size() > 0) {

        //BFMatcher matcher(NORM_L2);
        FlannBasedMatcher matcher;
        //std::vector<vector<DMatch> > matches;
        vector<DMatch> matches;
        //matcher.knnMatch(desc, prev_desc, matches, 2);
        matcher.match(desc, prev_desc, matches);


        //remove bad matches
        double max_dist = 0; double min_dist = 100;

        //-- Quick calculation of max and min distances between keypoints
        for (int i = 0; i < desc.rows; i++ ) {
            double dist = matches[i].distance;
            if (dist < min_dist) min_dist = dist;
            if (dist > max_dist) max_dist = dist;
        }

        //printf("-- Max dist : %f \n", max_dist );
        printf("-- Min dist : %f \n", min_dist );

        //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
        std::vector<DMatch> good_matches;

        for (int i = 0; i < desc.rows; i++ ) {
            if (matches[i].distance <= 3 * min_dist && good_matches.size() < 100) {
                good_matches.push_back(matches[i]);
            }
        }

        cout << "good_matches:\t" << good_matches.size() << endl;

        Mat img_matches;
        drawMatches(img_crop, kp, prev_img, prev_kp, good_matches, img_matches);
        //drawMatches(img_crop, kp, prev_img, prev_kp, match2, img_matches2);

        resize(img_matches, img_matches, Size(), 0.7, 0.7);
        imshow("img_matches", img_matches);


        //get transform
        vector<Point2f> prev, now;
        for (int i = 0; i < good_matches.size(); ++i) {
            now.push_back(kp[good_matches[i].queryIdx].pt);
            prev.push_back(prev_kp[good_matches[i].trainIdx].pt);
        }
        Mat H = findHomography(now, prev, CV_RANSAC);
        //cout << "H: \n" << H << endl;

        if (H.rows == 0) {
            H = Mat(3, 3, CV_32F);
        }

        //visualize transform
        if (kitten.data) {
            Mat kitten_T;
            warpPerspective(kitten, kitten_T, H.inv(), kitten.size());
            imshow("kitten", kitten_T);
        }

        double temp_K[9];
        for (int i = 0; i < 9; i++) {
            temp_K[i] = cam_msg->K[i];
        }
        Mat K = Mat(3, 3, CV_64FC1, temp_K);

        //update global pose

        double focal = 1.0;
        cv::Point2d pp(0.0, 0.0);
        Mat E, R, t, mask;
        //decomposeHomographyMat(H,K,R,t,N); // old method

        // Uses 5-point relative pose algorithm
        E = findEssentialMat(now, prev, focal, pp, RANSAC, 0.999, 1.0, mask);
        if (E.isContinuous()) {
            recoverPose(E, prev, now, R, t, focal, pp, mask);

            R.convertTo(R, CV_32F);
            t.convertTo(t, CV_32F);

            Mat c3 = cam_pos.col(3);
            c3 += t;

            Mat rot = cam_pos.colRange(0, 3), rot_vec, R_vec;
            Rodrigues(rot, rot_vec);
            Rodrigues(R, R_vec);

            rot_vec += R_vec;
            Mat rot2;
            Rodrigues(rot_vec, cam_pos.colRange(0, 3));


            string breakpoint;
        }



    }




    waitKey(1);

    prev_kp = kp;
    prev_desc = desc;
    prev_img = img_crop;
}


/**
 * @function readme
 */
void readme() { std::cout << " Usage: rosrun ascent_descent_localizer <cameraTopic>" << std::endl; }
