# Ascent Descent Localization #
## Darren Harton ##
The purpose of this project was to provide localization using data from a monocular camera as it moved up and down the water column in a marine environment. 
A video of the system running can be seen here: [https://www.youtube.com/watch?v=VeS-9OAadrs](https://www.youtube.com/watch?v=VeS-9OAadrs)

### Image Pipeline ###
The following is a summary of the sequential steps used in the solution for this problem.

### Raw Image: ###
The provided video files were converted into rosbag files containing compressed images as well as camera calibration data that was sourced from a separate project using GoPro stereo cameras.

### Rectified Image: ###
Images were rectified using the published calibration data with the image_proc ROS node.

### Cropped Image: ###
In initial tests, a large number of features were found along the black border of the rectified images. Cropping was applied to alleviate these false positives.

### Image Intensity Equalization: ###
An equalization function was applied to the image's intensity channel to make the faint features of the shipwreck in the images appear more vividly. This made a significant change in the number and quality of features found.

### Gaussian Blur: ###
As a result of the previous step, compression artifacts became obvious and were mistaken for features. Applying a Gaussian blur alleviates this issue.

### SURF Feature Detection: ###
Features were identified using the "upright" variant of the OpenCV SURF implementation. This was done to improve performance at no penalty since the difference between individual image frames is small. Early in the project, it was advantageous to select for features with higher octaves as these were more likely to belong to the shipwreck as opposed to fish or floating particles. This was later disabled, however, because more features were needed.

### Feature Matching: ###
A Flann-based matcher was used to match features between the current and previous images. It offered much better performance than the brute-force matcher with similar performance. Higher quality matches were selected by removing matches where the distance moved was greater than three times the minimum distance per match set.

### Essential Matrix: ###
The essential matrix was calculated using OpenCV's implementation of Nister's Five-Point Algorithm. The results of this can be viewed as a transformation being applied to a picture of a kitten. This was useful in evaluating the quality of the selected features and matches in real time.

### Global Pose: ###
The global pose was calculated by iteratively applying the calculated rotation and transformation matrices. This was obtained by finding the essential matrix using the matches between frames and RANSAC. The results of this step is visualized as a 3D cube.

### How to use ###
To run this application, first start roscore. 

Once the application has been moved into the catkin source directory and has been compiled, it can be started by running:

```
roslaunch ascent_descent_localization full.launch 
```


In another terminal, the rosbag file must be started. I recommend reducing the playback speed. On my own machine, consistent results were obtainable only when the playback was slowed using a value about 0.05.

If you wish to use a picture to visualize the homography matrix, change line 72 in ascent_descent_localization.cpp to point to kitten.jpeg in the images folder.
